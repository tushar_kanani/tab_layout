package com.javacodegeeks.android.tablayout_with_fragments;

import android.app.ActionBar.Tab;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ActionBar;
import android.util.Log;

public class TabListener implements ActionBar.TabListener {

    private Fragment fragment;
    String TAG  = "TATUBHA : TabListener";

    // The contructor.
    public TabListener(Fragment fragment) {
        Log.i(TAG,"selected fragment  : " + fragment);
        this.fragment = fragment;
    }

    // When a tab is tapped, the FragmentTransaction replaces
    // the content of our main layout with the specified fragment;
    // that's why we declared an id for the main layout.
    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        Log.i(TAG,"On Tab Select");
        //ft.show(fragment);
        //ft.attach(fragment);
        ft.replace(R.id.activity_main, fragment);

        MainActivity.active_tab = tab;
    }

    // When a tab is unselected, we have to hide it from the user's view.
    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
        Log.i(TAG,"On Tab unSelect");
        ft.remove(fragment);
       // ft.hide(fragment);
       // ft.detach(fragment);
    }

    // Nothing special here. Fragments already did the job.
    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
        Log.i(TAG,"On Tab reSelect");
        //ft.attach(fragment);

    }
}
