package com.javacodegeeks.android.tablayout_with_fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
    String TAG = "TATUBHA : MainActivity";
    // Declaring our tabs and the corresponding fragments.
    //ActionBar.Tab bmwTab;
    static public ActionBar.Tab active_tab;//, fordTab, toyotaTab;
   // Fragment bmwFragmentTab;// = new BmwFragmentTab();
   
    Button add_btn;
    Intent tIntent;
    static public boolean done;
    static ActionBar actionBar;ProgressDialog progress;
    Context ctx;
    static int maxTab;
    Intent intent;
    boolean val= false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        done = false;
        // Asking for the default ActionBar element that our platform supports.
        actionBar = getActionBar();

        // Screen handling while hiding ActionBar icon.
        actionBar.setDisplayShowHomeEnabled(false);

        // Screen handling while hiding Actionbar title.
        actionBar.setDisplayShowTitleEnabled(false);

        // Creating ActionBar tabs.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
       // bmwFragmentTab.setTabID(maxTab);
        // Setting custom tab icons.
      //  bmwTab = actionBar.newTab().setIcon(R.drawable.bmw_logo);
        //  toyotaTab = actionBar.newTab().setIcon(R.drawable.toyota_logo);
        // fordTab = actionBar.newTab().setIcon(R.drawable.ford_logo);
        add_btn = (Button) findViewById(R.id.add_button);
        // Setting tab listeners.
       // bmwTab.setTabListener(new TabListener(bmwFragmentTab));

        // toyotaTab.setTabListener(new TabListener(toyotaFragmentTab));
        // fordTab.setTabListener(new TabListener(fordFragmentTab));
        progress = new ProgressDialog(this);
        ctx = this;
        // Adding tabs to the ActionBar.

        //    actionBar.addTab(toyotaTab);
        //   actionBar.addTab(fordTab);
    }
    private void addNewTab(){
        ActionBar.Tab bmwTab;

        Fragment   bmwFragmentTab = new BmwFragmentTab();
        bmwTab = actionBar.newTab().setIcon(R.drawable.bmw_logo);
        bmwTab.setTabListener(new TabListener(bmwFragmentTab));

        actionBar.addTab(bmwTab);

    }
    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        if (done == true) {
           addNewTab();
            done = false;
        }
    }

    public void MainActivityOnClickListener(View v) {
        switch (v.getId()) {
            case R.id.add_button:
                if (maxTab < 6) {
                    new TestAsync().execute();

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Max Tab limit reached");
                    builder.show();
                }
                break;
        }
    }

    class TestAsync extends AsyncTask<Void, Integer, Boolean>
    {

        protected void onPreExecute (){
            progress = ProgressDialog.show(ctx, "TATUBHA",
                    "Serching for new device", true);
            checkNewDevice();
            Log.i(TAG,"PreExceute" + "On pre Exceute......");
        }

        protected Boolean doInBackground(Void...arg0) {
            Log.i(TAG, "DoINBackGround" + "On doInBackground...");

            try {

                Thread.sleep(6000);
            } catch (InterruptedException e) {
                Thread.interrupted();
            }
            return val;
        }

        protected void onProgressUpdate(Integer...a){
            Log.d(TAG,"You are in progress update ... " + a[0]);
        }

        protected void onPostExecute(Boolean result) {
            Log.i(TAG,"In post execution");
            val = checkConnection();
            progress.dismiss();
            Log.d(TAG,"val : "+result);

            if(val == true){
                Log.i(TAG,"Connection done!!!");
                maxTab++;
                intent = new Intent(ctx, config.class);
                startActivity(intent);
            }
        }

    }


    private boolean checkNewDevice() {
        Log.i(TAG, "Connecting to network......");
        WifiConfiguration wifiConfig = new WifiConfiguration();

        wifiConfig.SSID = "/sibguest/";
        wifiConfig.preSharedKey = "/Gu@st#200/";

        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);

        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();

        return val;
    }
    private boolean checkConnection() {
        Log.i(TAG, "checkConnection()");
        ConnectivityManager myConnManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo myNetworkInfo = myConnManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        WifiManager myWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo myWifiInfo = myWifiManager.getConnectionInfo();
        if (myWifiInfo != null) {
            Log.i(TAG,myWifiInfo.getSSID());

            if (! myWifiInfo.getSSID().equals("Tushar")) {
                Log.i(TAG, "Connected to tatubha");
                val = true;
                return true;

            } else{
                Log.i(TAG,"SSID didn't match");
                val = false;
                return false;
            }

        }else{
            Log.i(TAG,"wifiInfo is null");
        }
        return false;
    }

}
