package com.javacodegeeks.android.tablayout_with_fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Fragment;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class BmwFragmentTab extends Fragment {
    RelativeLayout rl;
    String TAG = "TATUBHA : TABactivity";
    int tabID;
    //Button btn[];// = bew Button();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bmw_layout, container, false);
        rl = (RelativeLayout)rootView.findViewById(R.id.rl);

        Log.i(TAG,"Root view rl :" + rl);
        this.tabID = MainActivity.maxTab;
        createBtn();
        return rootView;
    }
    private void createBtn(){
        Log.i(TAG,"#####In btn creat######");
        Button myButton = new Button(getActivity());
        myButton.setText("Add Me : " + tabID);
        // myButton.setId(1);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        myButton.setOnClickListener(btnclick);

        rl.addView(myButton, lp);

    }
    View.OnClickListener btnclick = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            Log.i(TAG,"BTN click listener for tab id  : " + tabID);

        }
    };





}
