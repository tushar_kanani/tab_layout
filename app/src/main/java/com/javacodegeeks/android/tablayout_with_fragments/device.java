package com.javacodegeeks.android.tablayout_with_fragments;

/**
 * Created by tushr on 29/4/16.
 */
public class device {
    private int id;
    private String location;

    public device(){}

    public device(String location, String author) {
        super();
        this.location = location;
     }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }


}
